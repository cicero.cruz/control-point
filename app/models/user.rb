class User < ApplicationRecord
  has_secure_password
  validates :name, presence: true
  validates_length_of :cpf, is: 11, uniqueness: true
  validates :email, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, length: { minimum: 8 }, if: -> { password.present? }
end
