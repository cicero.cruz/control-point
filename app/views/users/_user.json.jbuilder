json.extract! user, :id, :name, :cpf, :birth, :gender, :email, :status, :password, :role, :created_at, :updated_at
json.url user_url(user, format: :json)
