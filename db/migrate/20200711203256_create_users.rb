class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :cpf
      t.string :birth
      t.string :gender
      t.string :email
      t.string :status
      t.string :password
      t.string :role

      t.timestamps
    end
  end
end
